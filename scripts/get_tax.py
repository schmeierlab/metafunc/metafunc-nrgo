#!/usr/bin/env python
"""
NAME: script
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.2    20190513 Extract strain->species dict -> pickle
0.0.1    2019     Initial version.

LICENCE
=======
2019, copyright Sebastian Schmeier
s.schmeier@protonmail.com // https://www.sschmeier.com

template version: 2.0 (2018/12/19)
"""
import sys
import os
import argparse
import csv
import gzip
import bz2
import zipfile
import time
import subprocess
import pickle

__version__ = '0.0.2'
__date__ = '2019'
__email__ = 's.schmeier@protonmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset = Fore.RESET
    colors = {'success': Fore.GREEN,
              'error': Fore.RED,
              'warning': Fore.YELLOW,
              'info': ''}
except ImportError:
    sys.stderr.write('colorama lib desirable. ' +
                     'Install with "conda install colorama".\n\n')
    reset = ''
    colors = {'success': '', 'error': '', 'warning': '', 'info': ''}


def alert(atype, text, log, repeat=False):
    if repeat:
        textout = '{} [{}] {}\r'.format(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)
    else:
        textout = '{} [{}] {}\n'.format(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)

    log.write('{}{}{}'.format(colors[atype], textout, reset))
    if atype == 'error':
        sys.exit(1)


def success(text, log=sys.stderr):
    alert('success', text, log)


def error(text, log=sys.stderr):
    alert('error', text, log)


def warning(text, log=sys.stderr):
    alert('warning', text, log)


def info(text, log=sys.stderr, repeat=False):
    alert('info', text, log)


def parse_cmdline():
    """ Parse command-line args. """
    # parse cmd-line ----------------------------------------------------------
    description = 'Read delimited file.'
    version = 'version {}, date {}'.format(__version__, __date__)
    epilog = 'Copyright {} ({})'.format(__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='{}'.format(version))

    parser.add_argument(
        'str_file',
        metavar='RANKS-FILE',
        help='Delimited file. [use "-" or "stdin" to read from standard in]')
    parser.add_argument("str_file_kaiju",
                        metavar='KAIJU-FILE',
                        help='Kaiju nr-like file with proteins included in database. Subselect only proteins that are in this file and tax_ids in first file.')
    parser.add_argument(
        'str_file_prots',
        metavar='OUTFILE2',
        help='Output file protein ids.')
    parser.add_argument(
        'str_file_pickle',
        metavar='OUTFILE3',
        help='Output file strain to species pickle.')
    parser.add_argument('-d',
                        '--delimiter',
                        metavar='STRING',
                        dest='delimiter_str',
                        default='\t',
                        help='Delimiter used in input file.  [default: "tab"]')
    

    # if no arguments supplied print help
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.open(filename, 'rt')
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.ZipFile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def yield_line_gz_file(fn):
    """
    :param fn: String (absolute path)
    :return: GeneratorFunction (yields String)

    Reads gzip files line by line much faster then gzip.open().
    Alternatively use `zcat | python script.py`.
    Use:
         for line in yield_line_gz_file(fn):
             do_something(line)
    """
    ph = subprocess.Popen(["zcat", fn], stdout=subprocess.PIPE)
    for line in ph.stdout:
        line = line.decode("utf-8")
        yield line


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    try:
        fileobj = load_file(args.str_file)
    except IOError:
        error('Could not load file "{}". EXIT.'.format(args.str_file))

    # create outfile object
    if args.str_file_prots.split('.')[-1] == 'gz':
        outfileobj_p = gzip.open(args.str_file_prots, 'wt')
    else:
        outfileobj_p = open(args.str_file_prots, 'w')
        
    # delimited file handler
    csv_reader_obj = csv.reader(fileobj, delimiter=args.delimiter_str)

    group = ["species",
             "forma",
             "varietas",
             "no rank",  # if species name is != ''
             "subspecies",  # if species name is != ''
             "strain", #from on here added on April 28, 2021
             "serotype",
             "isolate",
             "forma specialis",
             "serogroup",
             "biotype",
             "genotype",
             "clade",
             "pathogroup",
             "morph"
    ]

    sys.stderr.write("Parsing ranks...\n")
    sys.stderr.flush()
    t00 = time.time()
    d = {}  # all ids of <=species-level
    for a in csv_reader_obj:
        # if no species name, do not use it
        if a[4] == '':
            continue
        else:
            rank = a[3]
            taxid = a[0]
            name = a[4]
            species = a[5]
            d[taxid] = (rank, name, species)
            if rank not in group:
                warning("Wrong rank encountered: {}. Ignored - rank still used as strain.".format(rank))


    sys.stderr.write("Done.\nParsed ranks in {} sec and {} taxids selected on <=species-level.\nParsing Kaiju-file...\n".format(time.time() - t00, len(d)))
    sys.stderr.flush()
    t00 = time.time()
    t01 = time.time()
    # read kaiju faa file
    i = 0
    d_out = {}
    secs = 0
    d_strain2species = {}
    for line in open(args.str_file_kaiju):
        if line[0] == ">":
            i+=1
            line = line.strip()
            taxid = line[1:].split('_')[-1]
            prot = '_'.join(line[1:].split('_')[:-1])  ## this is also the nr "main id"

            # we only consider proteins of <=species-level
            if taxid in d:
                outfileobj_p.write("{}\t{}\n".format(prot, taxid))
                species = d[taxid][2] 
                d_strain2species[taxid] = species  # strain 2 species dict
                
            #if i % 100000000 == 0:
            #    break

            if i % 1000000 == 0:
                t02 = time.time()-t01
                secs += t02
                sys.stderr.write("{} entries parsed. Last 1M done in {} sec. Total: {} secs\n".format(i, t02, secs))
                sys.stderr.flush()
                t01 = time.time()
                
    sys.stderr.write("Done\n")
    sys.stderr.write("Parsed kaiju-file in {} secs.\n".format(time.time() - t00))

    with open(args.str_file_pickle, 'wb') as handle:
        pickle.dump(d_strain2species, handle, protocol=pickle.HIGHEST_PROTOCOL)
        
    sys.stderr.flush()
    t00 = time.time()

    
    # ------------------------------------------------------
    outfileobj_p.close()
    return


if __name__ == '__main__':
    sys.exit(main())
