#!/usr/bin/env python
"""
NAME: get_nr.py
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

Reading from zcat is much faster then loading gzip in python:

    zcat nr.tab.gz | python get_nr.py db.sqlite - outfile


VERSION HISTORY
===============

0.0.1    2019    Initial version.

LICENCE
=======
2019, copyright Sebastian Schmeier
s.schmeier@protonmail.com // https://www.sschmeier.com

template version: 2.0 (2018/12/19)
"""
import sys
import os
import argparse
import csv
import gzip
import bz2
import zipfile
import time
import subprocess
import sqlite3
from sqlite3 import Error

__version__ = '0.0.1'
__date__ = '2019'
__email__ = 's.schmeier@protonmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset = Fore.RESET
    colors = {'success': Fore.GREEN,
              'error': Fore.RED,
              'warning': Fore.YELLOW,
              'info': ''}
except ImportError:
    sys.stderr.write('colorama lib desirable. ' +
                     'Install with "conda install colorama".\n\n')
    reset = ''
    colors = {'success': '', 'error': '', 'warning': '', 'info': ''}


def alert(atype, text, log, repeat=False):
    if repeat:
        textout = '{} [{}] {}\r'.format(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)
    else:
        textout = '\n{} [{}] {}\n'.format(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)

    log.write('{}{}{}'.format(colors[atype], textout, reset))
    log.flush()
    if atype == 'error':
        sys.exit(1)


def success(text, log=sys.stderr):
    alert('success', text, log)


def error(text, log=sys.stderr):
    alert('error', text, log)


def warning(text, log=sys.stderr):
    alert('warning', text, log)


def info(text, log=sys.stderr, repeat=False):
    alert('info', text, log)


def parse_cmdline():
    """ Parse command-line args. """
    # parse cmd-line ----------------------------------------------------------
    description = 'Read ids from file and extract GO-terms from sqlite database.'
    version = 'version {}, date {}'.format(__version__, __date__)
    epilog = 'Copyright {} ({})'.format(__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='{}'.format(version))
    parser.add_argument(
        'str_file_db',
        metavar='FILE',
        help='db.')
    parser.add_argument(
        'str_file',
        metavar='FILE',
        help='Delimited file with ids in rows delimited by comma. [Use "-" or "stdin" to read from stdin]')
    parser.add_argument('outfile_name1',
                        metavar='FILE',
                        help='Out-file GO.')
    parser.add_argument('outfile_name2',
                        metavar='FILE',
                        help='Out-file NR2GO.')
    parser.add_argument('--empty',
                        action="store_true",
                        default=False,
                        help='Include ids even if no GO-terms were found.')
    parser.add_argument('--verbose',
                        action="store_true",
                        default=False,
                        help='Print benchmarking messages.')

    # if no arguments supplied print help
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.open(filename, 'rt')
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.ZipFile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def create_connection(db_file):
    """ create a database connection to a SQLite database """
    try:
        conn = sqlite3.connect(db_file)
        #print(sqlite3.version)
        return conn
    except Error as e:
        print(e)

        
def yield_line_gz_file(fn):
    """
    :param fn: String (absolute path)
    :return: GeneratorFunction (yields String)

    Reads gzip files line by line much faster then gzip.open().
    Alternatively use `zcat | python script.py`.
    Use:
         for line in yield_line_gz_file(fn):
             do_something(line)
    """
    ph = subprocess.Popen(["zcat", fn], stdout=subprocess.PIPE)
    for line in ph.stdout:
        line = line.decode("utf-8")
        yield line


def max_sql_variables(max):
    """Get the maximum number of arguments allowed in a query by the current
    sqlite3 implementation.

    BASED ON: https://stackoverflow.com/questions/35616602/peewee-operationalerror-too-many-sql-variables-on-upsert-of-only-150-rows-8-c 

    Returns
    -------
    int
        inferred SQLITE_MAX_VARIABLE_NUMBER
    """
    import sqlite3
    db = sqlite3.connect(':memory:')
    cur = db.cursor()
    cur.execute('CREATE TABLE t (test)')
    low, high = 0, max
    while (high - 1) > low: 
        guess = (high + low) // 2
        query = 'INSERT INTO t VALUES ' + ','.join(['(?)' for _ in
                                                    range(guess)])
        args = [str(i) for i in range(guess)]
        try:
            cur.execute(query, args)
        except sqlite3.OperationalError as e:
            if "too many SQL variables" in str(e):
                high = guess
            else:
                raise
        else:
            low = guess
    cur.close()
    db.close()
    return low


def chunks(l, n):
    """Get chunks for a large list l with n number of elements, last list might be shorter"""
    n = max(1, n)
    return (l[i:i+n] for i in range(0, len(l), n))


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    # Check max allowed variables in sqlite query in current 
    # compiled sqlite version
    # use this number to build chunks if input data to big
    # To be secure minus 1
    SQLITE_MAX_VARIABLE_NUMBER = max_sql_variables(1000000) - 1 
    info("Maximum number of variables allowed in current sqlite3 installation: {}".format(SQLITE_MAX_VARIABLE_NUMBER+1))

    conn = create_connection(args.str_file_db)
    cur = conn.cursor()
        
    try:
        fileobj = load_file(args.str_file)
    except IOError:
        error('Could not load file "{}". EXIT.'.format(args.str_file))

    
    out_go = open(args.outfile_name1, "w")
    out_nr2go = open(args.outfile_name2, "w")
    
        
    # uniprot query
    sql1 = """ SELECT DISTINCT go.go from go
               INNER JOIN up2go ON go.id = up2go.go_id
               WHERE up2go.up IN ({}); """

    # query any id 
    sql2 = """ SELECT DISTINCT go.go FROM go 
               INNER JOIN up2go ON go.id = up2go.go_id 
               INNER JOIN id2up ON id2up.up = up2go.up 
               WHERE id2up.id IN ({}); """

    # verbose str benchmarking template
    str_out = ">{:10} GO-terms for {:10} lines ({:10} ids). " +\
              "Processed 100k lines in {:7.2f} sec. Total time (sec): {:7.2f}\r"
    
    # timing
    t00 = time.time()
    t0 = time.time()

    db_go_id = 0
    d_nr = {}
    d_go = {}
    
    i = 0
    j = 0
    num = 0
    num_found = 0
    items = []
    items_go = []
    for line in fileobj:
        ## line:
        ## main_id taxid redundant_id redundant_id ...
        line = [s.strip() for s in str(line).split(" ")]
        main_id = line[0]
        #taxid = line[1]  ## not needed right now
        ids1 = list(set( [main_id] + line[2:] ))

        # split all ids the first part in front of the "."
        ids2 = [s.split('.')[0].strip() for s in ids1]
        
        ids1 = tuple(ids1) # look for all ids
        ids2 = tuple(ids2) # look for up
        assert len(ids1) == len(ids2)


        # count number of ids searched  
        num += len(ids1)
        i += 1
        # benchmarking output if --verbose
        if args.verbose:
            if i % 100000 == 0:
                t1 = time.time() - t0
                sys.stderr.write(str_out.format(j, i, num, t1, time.time() - t00))
                sys.stderr.flush()
                t0 = time.time()

        # sqlite has an SQLITE_MAX_VARIABLE_NUMBER limit
        # here we try to circumvent by doing chunks of the max allowed size
        # to minimise the overhead in time for many queries
        list_go1 = []
        for id_list in chunks(ids2, SQLITE_MAX_VARIABLE_NUMBER):
            var_string = ",".join("?"*len(id_list))
            try:
                cur.execute(sql1.format(var_string), id_list)
            except sqlite3.OperationalError as e: 
                warning("Tried to use {} variables in SQL. Only {} allowed.".format(len(id_list), SQLITE_MAX_VARIABLE_NUMBER+1))
                raise
            resultsGO = cur.fetchall()
            list_go1 += list(resultsGO)

        list_go2 = []
        for id_list in chunks(ids1, SQLITE_MAX_VARIABLE_NUMBER):
            var_string = ",".join("?"*len(id_list))
            try:
                cur.execute(sql2.format(var_string), id_list)
            except sqlite3.OperationalError as e: 
                warning("Tried to use {} variables in SQL. Only {} allowed.".format(len(id_list), SQLITE_MAX_VARIABLE_NUMBER+1))
                raise
            resultsGO = cur.fetchall()
            list_go2 += list(resultsGO)

        # combine and make unique
        gos = list(set([g[0] for g in list_go1 + list_go2]))
        # if empty => next line
        if len(gos) == 0:
            continue 
        # else continue
        gos.sort()

        # count line to the ones with GOs
        j += 1
        num_found += len(ids1)

        # insert rows into db
        for g in gos:
            if g not in d_go:
                db_go_id += 1
                d_go[g] = db_go_id
                out_go.write("{}\t{}\n".format(db_go_id, g))
            # write nr2go entry
            out_nr2go.write("{}\t{}\n".format(main_id, d_go[g]))

            
    sys.stderr.write("\n>Done in {} sec.\n".format(time.time() - t00))
    sys.stderr.flush()

    #if args.verbose:
    t1 = time.time()
    secs = t1 - t00
    mins = round(secs / 60.)
    sys.stderr.write(">Found GO-terms for {} / {} lines and {} / {} ids.\n".format(j, i, num_found ,num))
    sys.stderr.write(">Finished in total time {} secs / ~{} min.\n".format(secs, mins))
        
    # ------------------------------------------------------
    conn.close()
    #outfileobj.close()
    return


if __name__ == '__main__':
    sys.exit(main())
