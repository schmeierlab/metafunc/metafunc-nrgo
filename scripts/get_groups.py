#!/usr/bin/env python
"""
NAME: script
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.2    20190513 Extract strain->species dict -> pickle
0.0.1    2019     Initial version.

LICENCE
=======
2019, copyright Sebastian Schmeier
s.schmeier@protonmail.com // https://www.sschmeier.com

template version: 2.0 (2018/12/19)
"""
import sys
import os
import argparse
import csv
import gzip
import bz2
import zipfile
import time
import subprocess
import pickle
import collections

__version__ = '0.0.2'
__date__ = '2019'
__email__ = 's.schmeier@protonmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset = Fore.RESET
    colors = {'success': Fore.GREEN,
              'error': Fore.RED,
              'warning': Fore.YELLOW,
              'info': ''}
except ImportError:
    sys.stderr.write('colorama lib desirable. ' +
                     'Install with "conda install colorama".\n\n')
    reset = ''
    colors = {'success': '', 'error': '', 'warning': '', 'info': ''}


def alert(atype, text, log, repeat=False):
    if repeat:
        textout = '{} [{}] {}\r'.format(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)
    else:
        textout = '{} [{}] {}\n'.format(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)

    log.write('{}{}{}'.format(colors[atype], textout, reset))
    if atype == 'error':
        sys.exit(1)


def success(text, log=sys.stderr):
    alert('success', text, log)


def error(text, log=sys.stderr):
    alert('error', text, log)


def warning(text, log=sys.stderr):
    alert('warning', text, log)


def info(text, log=sys.stderr, repeat=False):
    alert('info', text, log)


def parse_cmdline():
    """ Parse command-line args. """
    # parse cmd-line ----------------------------------------------------------
    description = 'Read delimited file.'
    version = 'version {}, date {}'.format(__version__, __date__)
    epilog = 'Copyright {} ({})'.format(__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='{}'.format(version))
    parser.add_argument(
        'str_file',
        metavar='OUTFILE',
        help='Output pickle file.')
    parser.add_argument(
        'files',
        nargs = "+",
        metavar='RANK-FILE',
        help='Delimited file.')

    # if no arguments supplied print help
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.open(filename, 'rt')
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.ZipFile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def yield_line_gz_file(fn):
    """
    :param fn: String (absolute path)
    :return: GeneratorFunction (yields String)

    Reads gzip files line by line much faster then gzip.open().
    Alternatively use `zcat | python script.py`.
    Use:
         for line in yield_line_gz_file(fn):
             do_something(line)
    """
    ph = subprocess.Popen(["zcat", fn], stdout=subprocess.PIPE)
    for line in ph.stdout:
        line = line.decode("utf-8")
        yield line


def main():
    """ The main function. """
    args, parser = parse_cmdline()
    
    d = collections.OrderedDict()
    for f in args.files:
        # delimited file handler
        csv_reader_obj = csv.reader(load_file(f), delimiter="\t")
        first_row = list(next(csv_reader_obj))
        g_taxid = first_row[0]
        g_group = first_row[1].split(';')[-1].strip()
        rank = first_row[3]
        d[g_taxid] = [g_group, rank, g_group, g_taxid]
        
        sys.stderr.write("Parsing file {} ...\n".format(f))
        sys.stderr.flush()
        t00 = time.time()  
        for a in csv_reader_obj:
            taxid = a[0]
            rank = a[3]
            name = a[1].split(';')[-1].strip()
            d[taxid] = [name, rank, g_group, g_taxid]
                
        sys.stderr.write("Done in {:5.2f} sec\n".format(time.time()-t00))
        sys.stderr.flush()
        
    sys.stderr.write("Pickling dict.\n")
    sys.stderr.flush()
    #with open(args.str_file, "w") as handle:
    #    for k in d:
    #        handle.write("{}\t{}\n".format(k, '\t'.join(d[k])))
    with open(args.str_file, 'wb') as handle:
        pickle.dump(d, handle, protocol=pickle.HIGHEST_PROTOCOL)
    sys.stderr.write("Done.\n")
    
    # ------------------------------------------------------
    return


if __name__ == '__main__':
    sys.exit(main())
