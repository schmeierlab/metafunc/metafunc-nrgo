#!/usr/bin/env python
"""
NAME: script
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.1    2019    Initial version.

LICENCE
=======
2019, copyright Sebastian Schmeier
s.schmeier@protonmail.com // https://www.sschmeier.com

template version: 2.0 (2018/12/19)
"""
import sys
import os
import argparse
import csv
import gzip
import bz2
import zipfile
import time
import subprocess
import re

__version__ = '0.0.1'
__date__ = '2019'
__email__ = 's.schmeier@protonmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset = Fore.RESET
    colors = {'success': Fore.GREEN,
              'error': Fore.RED,
              'warning': Fore.YELLOW,
              'info': ''}
except ImportError:
    sys.stderr.write('colorama lib desirable. ' +
                     'Install with "conda install colorama".\n\n')
    reset = ''
    colors = {'success': '', 'error': '', 'warning': '', 'info': ''}


def alert(atype, text, log, repeat=False):
    if repeat:
        textout = '{} [{}] {}\r'.format(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)
    else:
        textout = '{} [{}] {}\n'.format(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)

    log.write('{}{}{}'.format(colors[atype], textout, reset))
    if atype == 'error':
        sys.exit(1)


def success(text, log=sys.stderr):
    alert('success', text, log)


def error(text, log=sys.stderr):
    alert('error', text, log)


def warning(text, log=sys.stderr):
    alert('warning', text, log)


def info(text, log=sys.stderr, repeat=False):
    alert('info', text, log)


def parse_cmdline():
    """ Parse command-line args. """
    # parse cmd-line ----------------------------------------------------------
    description = 'Read delimited file.'
    version = 'version {}, date {}'.format(__version__, __date__)
    epilog = 'Copyright {} ({})'.format(__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='{}'.format(version))

    parser.add_argument(
        'str_file',
        metavar='NR-FILE',
        help='Delimited file. [use "-" or "stdin" to read from standard in]')
    parser.add_argument(
        "str_file_prot",
        metavar='PROT-FILE',
        help='Proteins to select from nr. Subselect only proteins that are in this file.')
    parser.add_argument(
        'str_file_out',
        metavar='OUTFILE',
        help='Output file prot ids.')

    # if no arguments supplied print help
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.open(filename, 'rt')
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.ZipFile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def yield_line_gz_file(fn):
    """
    :param fn: String (absolute path)
    :return: GeneratorFunction (yields String)

    Reads gzip files line by line much faster then gzip.open().
    Alternatively use `zcat | python script.py`.
    Use:
         for line in yield_line_gz_file(fn):
             do_something(line)
    """
    ph = subprocess.Popen(["zcat", fn], stdout=subprocess.PIPE)
    for line in ph.stdout:
        line = line.decode("utf-8")
        yield line


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    try:
        fileobj = load_file(args.str_file_prot)
    except IOError:
        error('Could not load file "{}". EXIT.'.format(args.str_file_prot))

    # create outfile object
    if args.str_file_out.split('.')[-1] == 'gz':
        outfileobj = gzip.open(args.str_file_out, 'wt')
    else:
        outfileobj = open(args.str_file_out, 'w')

    t00 = time.time()
    t01 = time.time()
    sys.stderr.write("Parsing prot-file...\n")
    sys.stderr.flush()
    # delimited file handler
    csv_reader_obj = csv.reader(fileobj, delimiter="\t")
    d = {}
    i = 0
    secs = 0
    for a in csv_reader_obj:
        i += 1
        if i % 1000000 == 0:
            t02 = time.time()-t01
            secs += t02
            sys.stderr.write("{:10} entries parsed. Last 1M done in {:7.2f} sec. Total: {:7.2f} secs\r".format(i, t02, secs))
            sys.stderr.flush()
            t01 = time.time()        
        d[a[0]] = None
        
    fileobj.close()
    sys.stderr.write("\nDone in {} secs.\n".format(time.time() - t00))
    sys.stderr.flush()

    # NR line
    # >XP_642131.1 hypothetical protein DDB_G0277827 [Dictyostelium discoideum AX4]P54670.1 RecName: Full=Calfumirin-1; Short=CAF-1BAA06266.1 calfumirin-1 [Dictyostelium discoideum AX2]EAL68086.1 hypothetical protein DDB_G0277827 [Dictyostelium discoideum AX4]
    ## \x01 delimits entries
    

    
    sys.stderr.write("Parsing nr-file...\n")
    sys.stderr.flush()
    t00 = time.time()
    t01 = time.time()
    secs = 0
    i = 0
    for line in yield_line_gz_file(args.str_file):
        if line[0] == ">":
            i+=1
            #if i % 100000000 == 0:
            #    break

            if i % 1000000 == 0:
                t02 = time.time()-t01
                secs += t02
                sys.stderr.write("{:10} entries parsed. Last 1M done in {:7.2f} sec. Total: {:7.2f} secs\r".format(i, t02, secs))
                sys.stderr.flush()
                t01 = time.time()

            res = [x.split(' ')[0].strip('>').strip() for x in line.split('\x01')]
            if res[0] in d:
                outfileobj.write("{}\n".format(','.join(res)))
            else:
                res2 = []
                for r in res:
                    if r in d:
                        res2.append(r)
                if len(res2) == 1:
                    res3 = [y.strip() for y in res if y not in res2]
                    res2 = res2 + res3
                    outfileobj.write("{}\n".format(','.join(res2)))
                elif len(res2) > 1:
                    sys.stderr.write("double accession in nr found in faa file: {}".format(",".join(res2)))
                    sys.exit()

                
    sys.stderr.write("\nDone\n")
    t03 = time.time() - t00
    sys.stderr.write("Parsed nr-file in {} sec / ~ {} min.\n".format(t03, round(t03 / 60.)))
    # ------------------------------------------------------
    return


if __name__ == '__main__':
    sys.exit(main())
