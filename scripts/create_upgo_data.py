#!/usr/bin/env python
"""
NAME: script
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.1    2019    Initial version.

LICENCE
=======
2019, copyright Sebastian Schmeier
s.schmeier@protonmail.com // https://www.sschmeier.com

template version: 2.0 (2018/12/19)
"""
import sys
import os
import argparse
import csv
import gzip
import bz2
import zipfile
import time
import subprocess
import pickle
csv.field_size_limit(sys.maxsize)

__version__ = '0.0.1'
__date__ = '2019'
__email__ = 's.schmeier@protonmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset = Fore.RESET
    colors = {'success': Fore.GREEN,
              'error': Fore.RED,
              'warning': Fore.YELLOW,
              'info': ''}
except ImportError:
    sys.stderr.write('colorama lib desirable. ' +
                     'Install with "conda install colorama".\n\n')
    reset = ''
    colors = {'success': '', 'error': '', 'warning': '', 'info': ''}


def alert(atype, text, log, repeat=False):
    if repeat:
        textout = '{} [{}] {}\r'.format(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)
    else:
        textout = '{} [{}] {}\n'.format(time.strftime('%Y%m%d-%H:%M:%S'),
                                        atype.rjust(7),
                                        text)

    log.write('{}{}{}'.format(colors[atype], textout, reset))
    if atype == 'error':
        sys.exit(1)


def success(text, log=sys.stderr):
    alert('success', text, log)


def error(text, log=sys.stderr):
    alert('error', text, log)


def warning(text, log=sys.stderr):
    alert('warning', text, log)


def info(text, log=sys.stderr, repeat=False):
    alert('info', text, log)


def parse_cmdline():
    """ Parse command-line args. """
    # parse cmd-line ----------------------------------------------------------
    description = 'Read delimited file.'
    version = 'version {}, date {}'.format(__version__, __date__)
    epilog = 'Copyright {} ({})'.format(__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument(
        '--version',
        action='version',
        version='{}'.format(version))
    parser.add_argument(
        'str_file_tax',
        metavar='FILE',
        help='Pickle file with dict of taxids.')
    parser.add_argument(
        'str_file',
        metavar='FILE',
        help='Delimited file. UNIPROT. (for ids)')
    parser.add_argument(
        'str_file2',
        metavar='FILE',
        help='Delimited file: GOA. (for GO-terms)')
    parser.add_argument(
        'str_file_out_go',
        metavar='FILE',
        help='Outfile-name')
    parser.add_argument(
        'str_file_out_up2go',
        metavar='FILE',
        help='Outfile-name')
    parser.add_argument(
        'str_file_out_id2up',
        metavar='FILE',
        help='Outfile-name')
    parser.add_argument(
        '--IEA',
        action="store_true",
        dest='no_iea',
        default=False,
        help="Do not use IEA annotated GO-terms.")

    # if no arguments supplied print help
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.open(filename, 'rt')
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.ZipFile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    try:
        d = pickle.load(open(args.str_file_tax, "rb"))
    except IOError:
        error('Could not load file "{}". EXIT.'.format(args.str_file_tax))

    set_tax = set(d)
    del(d)

        
    try:
        fileobj = load_file(args.str_file)
    except IOError:
        error('Could not load file "{}". EXIT.'.format(args.str_file))

    try:
        fileobj_goa = load_file(args.str_file2)
    except IOError:
        error('Could not load file "{}". EXIT.'.format(args.str_file2))

    oGO = open(args.str_file_out_go, "w")
    oUP2GO = open(args.str_file_out_up2go, "w")
    oID2UP = open(args.str_file_out_id2up, "w")

        
    # delimited file handler
    csv_reader_obj = csv.reader(fileobj, delimiter="\t")
        
    dGO = {}
    iGO = 0
    
    i = 0
    t00 = time.time()
    t0 = time.time()
    for a in csv_reader_obj:
        if a[6] == "":
            continue
        if a[12] not in set_tax:
            continue
        
        i += 1
        if i % 1000000 == 0:
            sys.stdout.write(">Parsed lines {:10}: Last 1M ran in {:7.2f} sec. Total time (sec): {:7.2f}\r".format(i, time.time() - t0, time.time() - t00))
            sys.stdout.flush()
            t0 = time.time()

        #if i == 500000:
        #    break

        up = a[0]
        # find ids and associate to up
        ids = []
        if a[3] != '':
            if a[3] != '-':
                for s2 in [s.strip() for s in a[3].split(";")]:
                    if s2 != '-':
                        ids.append(s2)
                    
        if a[17] != '':
            if a[17] != '-':
                for s2 in [s.strip() for s in a[17].split(";")]:
                    if s2 != '-':
                        ids.append(s2)

        for idx in ids:
            oID2UP.write("{}\t{}\n".format(idx, up))
        
    fileobj.close()

    j = 0
    sys.stdout.write("\n>Parsing GOA file:\n")
    sys.stdout.flush()
    # delimited file handler
    # GOA file
    csv_reader_obj = csv.reader(fileobj_goa, delimiter="\t")
    for a in csv_reader_obj:
        ## Uniprot?
        if a[0] != "UniProtKB":
            continue
        ## taxon:67581
        if a[12][6:] not in set_tax:
            continue
        if args.no_iea:  
            if a[6] == "IEA": # inferred from electronic annotation, skip
                continue
     
            
        j += 1
        if j % 1000000 == 0:
            sys.stdout.write(">Parsed GOA lines {:10}: Last 1M ran in {:7.2f} sec. Total time (sec): {:7.2f}\r".format(j, time.time() - t0, time.time() - t00))
            sys.stdout.flush()
            t0 = time.time()
                
        up = a[1]
        g = a[4]
        g = int(g[3:])
        if g not in dGO:
            iGO += 1
            dGO[g] = iGO
            oGO.write("{}\t{}\n".format(iGO, g))
            goid = iGO
        # already seen
        else:
            goid = dGO[g]

        oUP2GO.write("{}\t{}\n".format(up, goid))
    
    fileobj_goa.close()
    secs = time.time() - t00
    mins = round(secs / 60.)
    sys.stdout.write(">Final {} lines processed in {} sec / ~{} min\n".format(i+j, secs, mins))
    sys.stdout.flush()
    # ------------------------------------------------------
    return


if __name__ == '__main__':
    sys.exit(main())
