## =============================================================================
## WORKFLOW PROJECT: nrgo
## INIT DATE: 2019
import glob, os, os.path, datetime, sys, csv, urllib
from os.path import join, abspath
from snakemake.utils import validate, min_version

## For data download
from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider
FTP = FTPRemoteProvider()

## =============================================================================
## URLs
URL_GO        = "http://purl.obolibrary.org/obo/go/go-basic.obo"
URL_UP        = "ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/idmapping_selected.tab.gz"
URL_GOA       = "ftp.ebi.ac.uk/pub/databases/GO/goa/UNIPROT/goa_uniprot_all.gaf.gz"
URL_TAXONLIST = "https://raw.githubusercontent.com/bioinformatics-centre/kaiju/master/util/kaiju-taxonlistEuk.tsv"

## =============================================================================
## set minimum snakemake version #####
min_version("5.9.1")

## =============================================================================
## SETUP
## =============================================================================
## SET SOME DEFAULT PATH
DIR_SCRIPTS = abspath("scripts")
DIR_ENVS    = abspath("envs")
DIR_SCHEMAS = abspath("schemas")

# Specify configfile with --configfile
## LOAD VARIABLES FROM CONFIGFILE
## submit on command-line via --configfile
if config=={}:
    print('Please submit config-file with "--configfile <file>". EXIT.')
    sys.exit(1)
validate(config, schema=join(DIR_SCHEMAS, "config.schema.yaml"))
    
## define global Singularity image for reproducibility
## USE: "--use-singularity" to run all jobs in container
## without need to install any tools
singularity: "shub://sschmeier/metafunc-container:0.0.4"

DIR_BASE       = abspath(config["outdir"]) 
DIR_LOGS       = join(DIR_BASE, "logs")
DIR_BENCHMARKS = join(DIR_BASE, "benchmarks")
DIR_RES        = join(DIR_BASE, "results")

if not os.path.exists(config["kaiju_db"]):
    sys.stderr.write('Cannot open Kaiju database dir ' + \
                     '"{}" specified in config file. EXIT.\n'.format(config["kaiju"]["dir"]))
    sys.exit()
    
# check for files in Kaiju db dir
for f in ["nr_euk/nr.gz", "names.dmp", "nodes.dmp", "nr_euk/kaiju_db_nr_euk.faa"]:
    if not os.path.isfile(join(config["kaiju_db"], f)):
        sys.stderr.write('Cannot open file ' + \
                         '"{}" specified in dir {}. EXIT.\n'.format(f, config["kaiju_db"]))
        sys.exit()
        
FILE_NR  = join(config["kaiju_db"], "nr_euk/nr.gz")
FILE_FAA = join(config["kaiju_db"], "nr_euk/kaiju_db_nr_euk.faa")
DIR_TAXONOMY = config["kaiju_db"]


## this needs to happen first
## read taxonlist from URL
## extract taxids from first column of file
try:
    website = urllib.request.urlopen(URL_TAXONLIST)
except urllib.error.URLError as e:
    sys.stderr.write("Problems reading taxonlist. Reason: "+\
                     "{}. Check that url exists. EXIT.\n".format(e.reason))
    sys.exit()
website_txt = website.read().decode('utf-8')
taxids = [line.split("\t")[0].strip() for line in website_txt.split('\n')]
ranks  = [taxid for taxid in taxids if taxid != '']


## =============================================================================
## RULES
rule all:
    input:
        #rules.get_desc_dict.output,
        join(DIR_RES, "groups.pickle"),
        #rules.select_tax_prots.output.pickle,
        join(DIR_RES, "strain2species.pickle"),
        #rules.build_nrgo_db.output,
        join(DIR_RES, "nrgo.sqlite"),
        #rules.data_get_obo.output,
        join(DIR_RES, "go-basic.obo"),
        #rules.taxonlist.output
        join(DIR_RES, "tmp/kaiju-taxonlistEuk.tsv")


## Get the Kaiju taxonlist of included taxa
rule taxonlist:
    output:
        join(DIR_RES, "tmp/kaiju-taxonlistEuk.tsv")
    run:
        with open(output[0], "w") as out:
            for line in website_txt.split('\n'):
                out.write(f"{line}\n")
        

# Download some external data
rule data_uniprot:
    input:
        FTP.remote(URL_UP, keep_local=True)
    output:
        "data/external/idmapping_selected.tab.gz"
    benchmark:
        join(DIR_BENCHMARKS, "data_uniprot.txt")
    shell:
        "mv {input} {output}"


rule data_goa:
    input:
        FTP.remote(URL_GOA, keep_local=True)
    output:
        "data/external/goa_uniprot_all.gaf.gz"
    benchmark:
        join(DIR_BENCHMARKS, "data_goa.txt")        
    shell:
        "mv {input} {output}"

        
rule data_get_obo:
    output:
        join(DIR_RES, "go-basic.obo")
    priority: 20
    benchmark:
        join(DIR_BENCHMARKS, "data_get_obo.txt")
    shell:
        "wget {URL_GO} -O {output}"
        
  
## to build a descended dict per group
rule get_per_group:
    input:
        DIR_TAXONOMY
    output:
        temp(join(DIR_RES, "tmp/ranks/{rank}.txt.gz"))
    priority: 10
    benchmark:
        join(DIR_BENCHMARKS, "get_per_group_{rank}.txt")
    log:
        join(DIR_LOGS, "get_per_group_{rank}.log")
    conda:
        join(DIR_ENVS, "tax.yaml")
    threads: 1
    params:
        rank = "{rank}"
    shell:
        "taxonkit list "
        "--threads {threads} "
        "--data-dir {input} "
        "--ids {params.rank} "
        "--indent '' | "
        "egrep '[0-9]' | "
        "taxonkit lineage "
        "--threads {threads} "
        "--data-dir {input} --show-rank --show-lineage-taxids | "
        "taxonkit reformat "
        "--threads {threads} "
        "--data-dir {input} -f '{{s}}' --show-lineage-taxids | "
        "gzip > {output} 2> {log}"


## Build a dict for getting basic info for each taxid included
## e.g. taxid => name, rank, group, taxid (group)
rule get_desc_dict:
    input:
        #ranks=expand(join(DIR_RES, "tmp/ranks/{rank}.txt.gz"), rank=ranks)
        ranks=expand(rules.get_per_group.output, rank=ranks)
    output:
        join(DIR_RES, "groups.pickle")
    priority: 10
    benchmark:
        join(DIR_BENCHMARKS, "get_desc_dict.txt")
    log:
        join(DIR_LOGS, "get_desc_dict.log")
    params:
        script=join(DIR_SCRIPTS, "get_groups.py"),
        extra=r''
    shell:
        "python {params.script} {params.extra} {output} {input.ranks} > {log} 2>&1"


## Extract all protein ids for all tax to include from Kaiju, ie <=species-level
rule select_tax_prots:
    input:
        #ranks = expand(join(DIR_RES, "tmp/ranks/{rank}.txt.gz"), rank=ranks),
        ranks=expand(rules.get_per_group.output, rank=ranks),
        kaiju=FILE_FAA
    output:
        prots=temp(join(DIR_RES, "tmp/kaiju_proteins_tax.txt.gz")),  ## this will be used to find all ids from nr.gz
        pickle=join(DIR_RES, "strain2species.pickle")  ## a dict that connects all subspecies ids to species 
    benchmark:
        join(DIR_BENCHMARKS, "select_tax_prots.txt")
    log:
        join(DIR_LOGS, "select_tax_prots.log")
    params:
        script=join(DIR_SCRIPTS, "get_tax.py"),
        extra=r''
    shell:
        "zcat {input.ranks} | sort | uniq | "
        "python {params.script} {params.extra} - {input.kaiju} {output.prots} {output.pickle} "
        "> {log} 2>&1"

        
## subset proteins from nr and all its redundant ids for which to look for GO-terms
## ie these proteins belong to a taxid <= species-level
rule get_species_strains:
    input:
        nr=FILE_NR,
        prots=rules.select_tax_prots.output.prots
        #prots=join(DIR_RES, "tmp/kaiju_proteins_tax.txt.gz")
    output:
        temp(join(DIR_RES, "tmp/nr_ids2include.txt.gz")) 
    benchmark:
        join(DIR_BENCHMARKS, "get_species_strains.txt")
    log:
        join(DIR_LOGS, "get_species_strains.log")
    params:
        script=join(DIR_SCRIPTS, "get_nr_prots.py"),
        extra=r''
    shell:
        "python {params.script} {params.extra} {input.nr} {input.prots} {output} "
        "> {log} 2>&1"         


rule get_tax_prots_final:
    input:
        prots=rules.select_tax_prots.output.prots,
        #prots=join(DIR_RES, "tmp/kaiju_proteins_tax.txt.gz"),
        nr=rules.get_species_strains.output
        #nr=join(DIR_RES, "tmp/nr_ids2include.txt.gz")
    output:
        join(DIR_RES, "tmp/nr_ids.txt")  ## ids to look for GO-terms
    benchmark:
        join(DIR_BENCHMARKS, "get_tax_prots_final.txt")
    log:
        join(DIR_LOGS, "get_tax_prots_final.log")
    shell:
        "join <(zcat {input.prots} | sort -k1,1) <(zcat {input.nr} | sed 's/,/\t/g' | sort -k1,1) "
        "2> {log} > {output}"
        

## create a id to UP 2 GO data
rule build_upgo_data:
    input:
        taxids=rules.get_desc_dict.output,
        #taxids=join(DIR_RES, "groups.pickle"),
        up=rules.data_uniprot.output,
        #up="data/external/idmapping_selected.tab.gz",
        goa=rules.data_goa.output
        #goa="data/external/goa_uniprot_all.gaf.gz"
    output:
        go=join(DIR_RES, "tmp/upgo/go.txt"),
        up2go=temp(join(DIR_RES, "tmp/upgo/up2go.txt")),
        id2up=temp(join(DIR_RES, "tmp/upgo/id2up.txt"))
    benchmark:
        join(DIR_BENCHMARKS, "build_upgo_data.txt")
    log:
        join(DIR_LOGS, "build_upgo_data.log")
    params:
        script=join(DIR_SCRIPTS, "create_upgo_data.py"),
        #extra="--IEA"  # only consder non IEA ones, not much remains
        extra=r""
    shell:
        "python {params.script} {params.extra} {input.taxids} {input.up} {input.goa} {output.go} {output.up2go} {output.id2up} > {log} 2>&1"


rule sort_up2go:
    input:
        rules.build_upgo_data.output.up2go
    output:
        join(DIR_RES, "tmp/upgo/up2go.sorted.txt")
    benchmark:
        join(DIR_BENCHMARKS, "sort_up2go.txt")
    shell:
        "cat {input} | sort | uniq > {output}"

        
rule sort_id2up:
    input:
        rules.build_upgo_data.output.id2up
    output:
        join(DIR_RES, "tmp/upgo/id2up.sorted.txt")
    benchmark:
        join(DIR_BENCHMARKS, "sort_id2up.txt")
    shell:
        "cat {input} | sort | uniq > {output}"


rule build_upgo_load_script:
    input:
        rules.build_upgo_data.output.go,
        rules.sort_up2go.output,
        rules.sort_id2up.output
    output:
        join(DIR_RES, "tmp/load_upgo.sql")
    log:
        join(DIR_LOGS, "build_upgo_load_script.log")
    run:
        with open(output[0], "w") as out:
            out.write("""
CREATE TABLE IF NOT EXISTS id2up (
                                 id text NOT NULL,
                                 up text NOT NULL);
CREATE TABLE IF NOT EXISTS go (
                              id integer PRIMARY KEY,
	                      go integer NOT NULL);
CREATE TABLE IF NOT EXISTS up2go (
                                 up text NOT NULL,
                                 go_id integer NOT NULL,
                                 unique (up, go_id),
                                 FOREIGN KEY (go_id) REFERENCES go (id));
.mode csv
.separator "\\t"
.import {} go
.import {} up2go
.import {} id2up
CREATE INDEX up_idx ON up2go (up);
CREATE INDEX id_idx ON id2up (id);
            """.format(input[0], input[1], input[2]))

        
## create a id to UP 2 GO db
rule build_upgo_db:
    input:
        rules.build_upgo_load_script.output
    output:
        join(DIR_RES, "tmp/upgo/upgo.sqlite")
    benchmark:
        join(DIR_BENCHMARKS, "build_upgo_db.txt")
    log:
        join(DIR_LOGS, "build_upgo_db.log")
    conda:
        join(DIR_ENVS, "sqlite.yaml")
    shell:
        "cat {input} | sqlite3 {output} > {log} 2>&1"
        

## Extract for all redundant ids a unique set of GO-terms
## that will be associated in the db to the nr "main id"
rule get_nr_go_data:
    input:
        ## a list of ids. All ids in a row belong together with the first being the "main id"
        ## second col is tax
        ## 3col - N all other ids
        nr=rules.get_tax_prots_final.output,
        #nr=join(DIR_RES, "tmp/nr_ids.txt"),
        # uniprot id to go db
        db=rules.build_upgo_db.output
        #db=join(DIR_RES, "tmp/upgo/upgo.sqlite")
    output:
        go=join(DIR_RES, "tmp/nrgo/go.txt"),
        nr2go=join(DIR_RES, "tmp/nrgo/nr2go.txt")
    benchmark:
        join(DIR_BENCHMARKS, "get_nr_go_data.txt")
    log:
        join(DIR_LOGS, "get_nr_go_data.log")
    conda:
        join(DIR_ENVS, "sqlite.yaml")
    params:
        script=join(DIR_SCRIPTS, "create_nrgo_data.py"),
        extra="--verbose"
    shell:
        # zcat is much faster than reading gzip in python
        # now we are using only uncompressed here to speed it up
        "cat {input.nr} | python {params.script} {params.extra} {input.db} - {output.go} {output.nr2go} > {log} 2>&1"


rule build_nrgo_load_script:
    input:
        rules.get_nr_go_data.output.go,
        rules.get_nr_go_data.output.nr2go
    output:
        join(DIR_RES, "tmp/load_nrgo.sql")
    log:
        join(DIR_LOGS, "build_nrgo_load_script.log")
    run:
        with open(output[0], "w") as out:
            out.write("""
CREATE TABLE IF NOT EXISTS go (
                               id integer PRIMARY KEY,
                               goid integer NOT NULL);
CREATE TABLE IF NOT EXISTS nr2go (
                                  nr_id text NOT NULL,
                                  go_id integer NOT NULL,
                                  FOREIGN KEY (go_id) REFERENCES go (id));
.mode csv
.separator "\\t"
.import {} go
.import {} nr2go
CREATE INDEX nrid_idx1 ON nr2go (nr_id);
CREATE INDEX nrid_idx2 ON nr2go (go_id);
            """.format(input[0], input[1]))
        
        
## create a id to nrgo db via sql-script
rule build_nrgo_db:
    input:
        rules.build_nrgo_load_script.output
    output:
        join(DIR_RES, "nrgo.sqlite")
    benchmark:
        join(DIR_BENCHMARKS, "build_nrgo_db.txt")
    log:
        join(DIR_LOGS, "build_nrgo_db.log")
    conda:
        join(DIR_ENVS, "sqlite.yaml")
    shell:
        "cat {input} | sqlite3 {output} > {log} 2>&1"




