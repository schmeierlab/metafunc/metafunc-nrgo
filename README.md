# PROJECT: nrgo

- AUTHOR: Sebastian Schmeier (s.schmeier@pm.me)
- AUTHOR: Arielle Sulit 
- DATE: 2019 


## Details

This workflow will create a `nrgo.sqlite` sqlite-database, a `strain2species` and `groups` pickled dictionary that are needed to run a workflow to associate [Kaiju](http://kaiju.binf.ku.dk) results 
to functional categories.

The created `nrgo.sqlite` database is of the following structure:


```
CREATE TABLE IF NOT EXISTS go (
                               id integer PRIMARY KEY,
                               goid integer NOT NULL
                              );

CREATE TABLE IF NOT EXISTS nr2go (
                                  nr_id text NOT NULL,
                                  go_id integer NOT NULL,
                                  FOREIGN KEY (go_id) REFERENCES go (id)
                                 );
```

## Requirements

- Maximum memory usage: ~18GB
- Temporary disk space usage while building databases: ~80GB
- Final disk space usage: ~18GB (after deleting all tmp files)
- Internet connection
- Run-time: ~4hr with 12 cores
  * Required data download not included in timing (depends on your inet connection, it can take long...)
  * Two files each ~8.5GB need to be downloaded by the workflow
  

## Usage

### 1. Building Kaiju database

First one needs to build a `nr_euk`  [Kaiju](Http://Kaiju.Binf.Ku.Dk) database. This can be accomplished
with:

```bash
kaiju-makedb -s nr_euk -t 6
```

Do not delete just yet any files created by `kaiju-makedb`, even if they are not
necessary to run `kaiju`. We will use some of those files to build the nrgo
database for our purpose.


### 2. Install workflow


#### 2.1 Install miniconda

```bash
# LINUX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh

# MACOSX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh
```

#### 2.2 Install Snakemake

```bash
conda install snakemake>5.10.0
```

#### 2.3 Install workflow


```bash
git clone https://gitlab.com/schmeierlab/metafunc/metafunc-nrgo.git
```


#### 2.4 Execute workflow

###### Adjust config.yaml

Currently it is required to submit config parameters via `--configfile config.yaml`.
Change `config.yaml` to point to the correct  [Kaiju](Http://Kaiju.Binf.Ku.Dk) `nr_euk` database directory.


##### Run workflow

The workflow can be executed in different "modes", depending if certain 3-rd party software is installed on the system.
In particular Singularity (virtualisation software) makes the workflow more reproducibile but might not be readily installed on all systems.
In that case use the "conda-only mode".

```bash
### Do a dryrun of the workflow, show rules, order, and commands
snakemake -np --configfile config.yaml

### Recommended singularity-only mode for reproducibility
# No tool download needed. Singularity container will be pulled and used for the worklow.
# Only works on systems where Singularity has been installed.
snakemake -p --use-singularity --cores 12 --configfile config.yaml > run.log 2>&1

# If necessary bind more folders for singularity outside of your home.
snakemake -p --use-singularity --cores 12 --singularity-args "--bind /mnt/disk2" --configfile config.yaml > run.log 2>&1

### Conda-only run
# Will download tools and store them in environment files in the .snakemake/conda dir
snakemake -p --use-conda --cores 12 --configfile config.yaml > run.log 2>&1

### 
# Show a detailed summary of the produced files and used commands
snakemake -D --configfile config.yaml
```


### Data


#### Taxonomy

The workflow will be using the taxonomy files that were used to create the  [Kaiju](Http://Kaiju.Binf.Ku.Dk)
`nr_euk` database.


#### NR

The workflow will be using the `nr.gz` file that was used to create the  [Kaiju](Http://Kaiju.Binf.Ku.Dk)
`nr_euk` database.


#### Uniprot

The workflow will automatically download the needed file `idmapping_selected.tab.gz` from: `ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/idmapping_selected.tab.gz` and place it in `./data/external/`:

- Should you want to update this file at a later stage, remove it, the workflow will re-download the latest version upon execution.
- If you want to use a version you have already downloaded, please place the file with the exact (`idmapping_selected.tab.gz`) name in that directory.
- This file will be used to extract protein identifiers and map them to UniProt ids.


#### EBI Gene Ontology Annotation (GOA)

The workflow will download the EBI GOA annotation file from: `ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/UNIPROT/goa_uniprot_all.gaf.gz` and place it in `./data/external/`:

- Should you want to update this file at a later stage, remove it, the workflow will re-download the latest version upon execution.
- If you want to use a version you have already downloaded, please place the file with the exact (`goa_uniprot_all.gaf.gz`) name in that directory.
- This file will be used to extract GO-terms for UniProt proteins.


#### Kaiju taxonlist

The workflow will automatically parse the online taxonlist file at `https://raw.githubusercontent.com/bioinformatics-centre/kaiju/master/util/kaiju-taxonlistEuk.tsv`.
It will be stored in the result `tmp` directory so that this information can be accessed later on.


## Results

A set of files that will be used by a different workflow to associate function to microbes.

- **groups.pickle**
  * Information about a taxid's parent.
- **strain2species.pickle**
  * Mapping of taxids of rank <= species to species
- **nrgo.sqlite**
  * The main database that associates NR "main-id" to GO-terms.
- **goslim_generic.obo**
- **go-basic.obo**
- tmp/kaiju-taxonlistEuk.tsv
  * The taxa used by Kaiju for building a `nr_euk` database.

All files in the `[outdir]/tmp` can be deleted as they are not required for
subsequent workflows.

